$(document).ready(function(){
	$("#demosMenu").change(function(){
	  window.location.href = $(this).find("option:selected").attr("id") + '.html';
	});
});



$(document).ready(function() {
	/*page loading*/
	var windowSize = $(window).width(); // Could've done $(this).width()
			    

			    	var loaderHide = function() {
			 

						  $('.loader').delay(500).animate({
										opacity: '0'
									}, 500, 'easeInExpo');
						   $(".loader").hide();

						   if(windowSize >= 768){
						   		$("#fullpage").show();

						   	}
						}

						window.onload = function() {
						  setTimeout(loaderHide, 3200);
						}

			  
			var dots = 0;
    

    			setInterval (type, 600);


				function type()
				{
				    if(dots < 3)
				    {
				        $('#dots').append('.');
				        dots++;
				    }
				    else
				    {
				        $('#dots').html('');
				        dots = 0;
				    }
				}


		

   /*full page parallax*/
			$('#fullpage').fullpage({
				sectionsColor: ['#F1E9F1', '#FFFFFF', '#54667A', '#F1F9FD', '#E3D4E4', '#DCD9D7', '#E1C797', '#F1F9FD', '#F5E8E0', '#FBE9AA', '#EBE6E6', '#EDEDED', '#00384F', '#FFFFFF'],
				anchors: ['firstPage', 'secondPage', '3rdPage', '4thpage', '5thpage', '6thpage', '7thpage', '8thpage', '9thpage', '10thpage', '11thpage', '12thpage', '13thpage', '14thpage','lastPage'],
				menu: '#menu',
				responsiveWidth: 768,

				afterLoad: function(anchorLink, index){

					//section 1
					
					$('.yearSlide img.img2k17').delay(600).animate({
							opacity: '1'
						}, 1500, 'easeInExpo');

					$('.yearSlide .allAddedUp').delay(1200).animate({
							opacity: '1'
						}, 2000, 'easeInExpo');
					


				
					//section 2
					if(index == 2){

					
					//fading in bg image
					var myVar2;

					function FadeSlide2() {
					    myVar2 = setTimeout(fadeImg, 500);
					}

					function fadeImg() {
					   $('#section1').addClass("section1ImageFade")
					}


					FadeSlide2();

					$('#section1 .intro').delay(1000).animate({
							opacity: '1'
						}, 1500, 'easeInExpo');

						$('#section1 .officeExpansion').delay(2000).animate({
							opacity: '1'
						}, 1500, 'easeInExpo');

						$('#section1 .secondary').delay(3000).animate({
							opacity: '1'
						}, 1000, 'easeInExpo');

					}

					//section 3
					if(anchorLink == '3rdPage'){
						//moving the image
						$('#section2').find('.intro').delay(2000).animate({
							opacity: '1'
						}, 1500, 'easeInExpo');

						$(".faded").each(function(i) {
						  $(this).delay(i * 1000).fadeIn(1000);
						});


					}

					//section 4
					if(index == 4){

						//fading in bg image
					var myVar3, myVar4

					function FadeSlide3() {
					    myVar3 = setTimeout(fadeImg2, 100);
					    myVar4 = setTimeout(fadeImg3, 2000);

					}

					function fadeImg2() {
					   $('#section3').addClass("section3ImageFade")
					}

					function fadeImg3() {
					   $('#section3').addClass("section3ImageFade2")
					}
						

					FadeSlide3();

					$('#section3').find('.intro').delay(1500).animate({
							opacity: '1'
						}, 1500, 'easeInExpo');

					}

					//section 5

					if(index == 5){

						$('#section4').animate({
							opacity: '1'
						}, 800, 'easeInExpo');

						$(".certificate").each(function(i) {

						  $(this).delay(i * 400).fadeIn(800);
						});

						$('#section4 .secondary').animate({
							opacity: '1'
						}, 3000, 'easeInExpo');

					}

					//section 6
					if(index == 6){

						//fading in bg image
						var myVar2;
						var myVar3; 
						function FadeSlide() {
						    myVar2 = setTimeout(fadeImg, 1000);
						}

						function fadeImg() {
						   $('#section5').addClass("section5ImageFade")
						}

						FadeSlide();

						$('#section5 .intro .visited').animate({
							opacity: '1'
						}, 2000, 'easeInExpo');

						$('.nineteen').animate({
							opacity: '1'
						}, 3000, 'easeInExpo');

						$('.thirty').animate({
							opacity: '1'
						}, 4000, 'easeInExpo');

						

						$('#section5 .secondary').animate({
							opacity: '1'
						}, 5000, 'easeInExpo');


						var terms = ["Argentina","Australia","Canada","Chile","Costa Rica","Czech Republic","France","Germany","Iceland","Ireland","Italy","Japan","Mexico","Netherlands","Seychelles","SouthAfrica","Spain","Thailand","Uruguay","Alabama","Alaska","Arizona","Arkansas","California","Colorado","Florida","Hawaii","Illinois","Indiana","Kansas","Kentucky","Louisiana","Massachusetts","Missouri","Mississippi","Nevada","New Mexico","New York","North Carolina","Ohio","Oregon","Pennsylvania","South Carolina","South Dakota","Tennessee","Texas","Utah","Virginia","Wisconsin"]; //array of terms to rotate
						var termscount = terms.length;
							function rotateTerm() {
								var ct = $("#rotate").data("term") || 0;
								if (ct == termscount) return;
								$("#rotate").data("term", ct == terms.length -1 ? 0 : ct + 1).text(terms[ct]).fadeIn().delay(100).fadeOut(200, rotateTerm);
							}
						
						rotateTerm();

						function textRotate(){
							myVar3 = setTimeout(rotateTerm, 2500);
						}


					}

					if(index == 7){


							//counting coffee cups
						var myVar2;
						
						function CafeSlide() {
						    myVar2 = setTimeout(fadeCoffee, 500);
						}

						function fadeCoffee() {
						   
							var coffeeLength = $("#popContainer li").length;

							if (coffeeLength < 70){
								var popContainer = $('#popContainer');

								// add cups
								var addCups = (num) => {
									for (let i = 0; i < num; i++) {
										setTimeout(function() {
											let person = document.createElement('li');
											person.innerHTML = '<span>' + '<img src="images/cortado.png" />' + '</span>';
											
											popContainer.append(person);
										}, 70 * i); 
									}
								}

								addCups(70);

							}
							

						}

						CafeSlide();

						$('#section6 .intro').animate({
							opacity: '1'
						}, 3000, 'easeInExpo'); 

						$('#section6 .secondary').animate({
							opacity: '1'
						}, 6000, 'easeInExpo');

					}


					//social media slide
					if(index == 8){

						$('#section7 .line').animate({
							opacity: '1'
						}, 1000, 'easeInExpo'); 

						$('#section7 .instagramPanel').animate({
							opacity: '1'
						}, 2500, 'easeInExpo'); 

						$('#section7 .twitterPanel').animate({
							opacity: '1'
						}, 4500, 'easeInExpo'); 


					}

					//CSA

					if(index == 9){

						$('#section8 .secondary').animate({
							opacity: '1'
						}, 2500, 'easeInExpo'); 

						$('#section8 .family').animate({
							opacity: '1'
						}, 1000, 'easeInExpo'); 

						$('#section8 .cloud').animate({
							opacity: '1'
						}, 4000, 'easeInExpo'); 


					}

					if(index == 10){

						$('#section9 .programmer').animate({
							opacity: '1'
						}, 1000, 'easeInExpo'); 
						//fading in bg image
						var myVar2;

						function FadeSlide2() {
					   		myVar2 = setTimeout(fadeImg, 2000);
						}

						function fadeImg() {
					   		$('#section9').addClass("section9ImageFade")
						}


						FadeSlide2();

						$('#section9 .litmus').animate({
							opacity: '1'
						}, 4000, 'easeInExpo'); 

						$('#section9 .code').animate({
							opacity: '1'
						}, 5500, 'easeInExpo'); 

						$('#section9 .slack').animate({
							opacity: '1'
						}, 6000, 'easeInExpo'); 



					}

					if(index == 11){

							//fading in bg image
					var myVar3

					function FadeSlide3() {
					    myVar3 = setTimeout(fadeImg2, 1000);
					   

					}

					function fadeImg2() {
					   $('.section10').addClass("section10ImageFade")
					}

				

					FadeSlide3();

					$('#section10 .earth').animate({
							opacity: '1'
					}, 3000, 'easeInExpo'); 

					$('#section10 .intro').animate({
							opacity: '1'
					}, 6000, 'easeInExpo'); 

					$('#section10 .steps-cloud').animate({
							opacity: '1'
					}, 6000, 'easeInExpo'); 



					}

					if(index == 12){

						$(".mail img").each(function(i) {
						  $(this).delay(i * 1000).fadeIn(1000);
						});

						$('#section11 .secondary').animate({
							opacity: '1'
						}, 4000, 'easeInExpo'); 


					}

					if(index == 13){

						//fading in bg image
							var myVarX;

							function FadeSlideX() {
							    myVarX = setTimeout(fadeImgX, 1000);
							}

							function fadeImgX() {
							   $('.section12').addClass("section12imageFade")
							}


							FadeSlideX();


							$('#section12 .numero').animate({
								opacity: '1'
							}, 3000, 'easeInExpo'); 

							$('#section12 .big-earth').animate({
								bottom: '-5px'
							}, 5000, 'easeInExpo'); 

							$('#section12 .email-cloud').animate({
								opacity: '1'
							}, 7000, 'easeInExpo'); 

					}

					if(index == 14){

						$("#menu").hide();
					}
					else{
						$("#menu").show();
					}


				}
			});




		});